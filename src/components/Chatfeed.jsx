import Mymessage from "./Mymessage";
import Messageform from "./Messageform";
import Theirmessage from "./Theirmessage";

const Chatfeed = (props) => {
  const { chats, activeChat, messages, userName } = props;
  const chat = chats && chats[activeChat];

  const renderReadReceipts = (message, isMyMessage) =>
    chat.people.map(
      (person, index) =>
        person.last_read === message.id && (
          <div
            key={`read_${index}`}
            className="read-receipt"
            style={{
              float: isMyMessage ? "right" : "left",
              backgroundImage:
                person.person.avatar && `url(${person.person.avatar})`,
            }}
          />
        )
    );

  const renderMessage = () => {
    const keys = Object.keys(messages);

    return keys.map((key, index) => {
      const message = messages[key];
      const lastMessage = index === 0 ? null : keys[index - 1];
      const myMessage = userName === message.sender.username;

      return (
        <div key={`msg_${index}`} style={{ width: "100%" }}>
          <div className="message-block">
            {myMessage ? (
              <Mymessage message={message} />
            ) : (
              <Theirmessage
                message={message}
                lastMessage={message[lastMessage]}
              />
            )}
          </div>
          <div
            className="read-receipts"
            style={{
              marginRight: myMessage ? "18px" : "0px",
              marginLeft: myMessage ? "0px" : "68px",
            }}
          >
            {renderReadReceipts(message, myMessage)}
          </div>
        </div>
      );
    });
  };

  if (!chat) return "Loading...";

  return (
    <div className="chat-feed">
      <div className="chat-title-container">
        <div className="chat-title">{chat.title}</div>
        <div className="chat-subtitle">
          {chat.people.map((person) => person.person.username)}
        </div>
      </div>
      {renderMessage()}
      <div style={{ height: "100px" }} />
      <div className="message-form-container">
        <Messageform {...props} chatId={activeChat} />
      </div>
    </div>
  );
};

export default Chatfeed;
