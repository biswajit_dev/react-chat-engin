import React from "react";
import { ChatEngine } from "react-chat-engine";

import Chatfeed from "./components/Chatfeed";
import LoginForm from './components/Loginform';
import "./App.css";

const App = () => {
    if (!localStorage.getItem('username')) return <LoginForm />;
    return(
        <ChatEngine 
            height="100vh"
            projectID="b4df089c-b0d1-448c-ba0c-5bf13f12d5a6"
            userName={localStorage.getItem('username')}
            userSecret={localStorage.getItem('password')}
            renderChatFeed={(chatAppProps) => <Chatfeed {...chatAppProps} />}
        />
    )
}

export default App;